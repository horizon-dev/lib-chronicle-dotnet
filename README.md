# Chronicle Platform .NET Core Library

> Lifetime tracking for physical artefacts.

Common .NET code for the Chronicle platform including clients.

## Requirements

* Required
    * [.NET Core](https://dotnet.microsoft.com/download) (>2.2)

## Release History

* 0.0.1
    * Work in progress

## Meta

© 2019 Horizon Digital Economy Research
[www.horizon.ac.uk](https://www.horizon.ac.uk)

[https://bitbucket.org/horizon-dev/lib-chronicle-dotnet](
https://bitbucket.org/horizon-dev/lib-chronicle-dotnet)

## Contributing

1. Fork it (<https://bitbucket.org/horizon-dev/lib-chronicle-dotnet>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
