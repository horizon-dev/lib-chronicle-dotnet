using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Crypto = Org.BouncyCastle.Crypto;

namespace Chronicle.Api.V1 {

    public class RsaMethods {

        private readonly HttpClient client;

        private readonly Crypto.Parameters.RsaPrivateCrtKeyParameters key;

        private readonly string baseUrl;

        private readonly string username;

        public RsaMethods(
                HttpClient client,
                Crypto.Parameters.RsaPrivateCrtKeyParameters key,
                string baseUrl,
                string username) {
            this.client = client;
            this.key = key;
            this.baseUrl = baseUrl;
            this.username = username;
        }

        public JObject CreateTimeline(
                string shortDescription,
                string longDescription = "") {

            var url = String.Format("{0}timeline", baseUrl);
            var method = "post";
            var query = new Dictionary<string, string>();
            var bodyDict = new Dictionary<string, string>();
            bodyDict.Add("shortDescription", shortDescription);
            bodyDict.Add("longDescription", longDescription);
            var body = JsonConvert.SerializeObject(bodyDict);
            var nonce = Auth.Utils.generateNonce();
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var signature = Auth.RsaSigner.sign(
                    key,
                    method,
                    url,
                    query,
                    username,
                    body,
                    nonce,
                    timestamp);

            var authHeader = CreateAuthHeader(nonce, signature, timestamp);

            var content = new StringContent(
                    body, System.Text.Encoding.UTF8, "application/json");
            var req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Headers.Add("Authorization", authHeader);
            req.Content = content;
            var response = client.SendAsync(req).GetAwaiter().GetResult();
            if (response.StatusCode != HttpStatusCode.Created) {
                throw new Exception(response.ReasonPhrase);
            }
            var responseBody = response.Content.ReadAsStringAsync()
                    .GetAwaiter().GetResult();
            return JObject.Parse(responseBody);
        }

        public JArray ReadTimelines() {

            var url = String.Format("{0}timeline", baseUrl);
            var method = "get";
            var query = new Dictionary<string, string>();
            var body = "";
            var nonce = Auth.Utils.generateNonce();
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var signature = Auth.RsaSigner.sign(
                    key,
                    method,
                    url,
                    query,
                    username,
                    body,
                    nonce,
                    timestamp);

            var authHeader = CreateAuthHeader(nonce, signature, timestamp);

            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add("Authorization", authHeader);
            var response = client.SendAsync(req).GetAwaiter().GetResult();
            if (response.StatusCode != HttpStatusCode.OK) {
                throw new Exception(response.ReasonPhrase);
            }
            var responseBody = response.Content.ReadAsStringAsync()
                    .GetAwaiter().GetResult();
            return JArray.Parse(responseBody);
        }

        public JObject ReadTimeline(long id) {

            var url = String.Format("{0}timeline/{1}/", baseUrl, id);
            var method = "get";
            var query = new Dictionary<string, string>();
            var body = "";
            var nonce = Auth.Utils.generateNonce();
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var signature = Auth.RsaSigner.sign(
                    key,
                    method,
                    url,
                    query,
                    username,
                    body,
                    nonce,
                    timestamp);

            var authHeader = CreateAuthHeader(nonce, signature, timestamp);

            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add("Authorization", authHeader);
            var response = client.SendAsync(req).GetAwaiter().GetResult();
            if (response.StatusCode != HttpStatusCode.OK) {
                throw new Exception(response.ReasonPhrase);
            }
            var responseBody = response.Content.ReadAsStringAsync()
                    .GetAwaiter().GetResult();
            return JObject.Parse(responseBody);
        }

        public JObject CloneTimeline(long id) {

            var url = String.Format("{0}timeline/{1}/clone/", baseUrl, id);
            var method = "post";
            var query = new Dictionary<string, string>();
            var body = "";
            var nonce = Auth.Utils.generateNonce();
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var signature = Auth.RsaSigner.sign(
                    key,
                    method,
                    url,
                    query,
                    username,
                    body,
                    nonce,
                    timestamp);

            var authHeader = CreateAuthHeader(nonce, signature, timestamp);

            var content = new StringContent(
                    body, System.Text.Encoding.UTF8, "application/json");
            var req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Headers.Add("Authorization", authHeader);
            req.Content = content;
            var response = client.SendAsync(req).GetAwaiter().GetResult();
            if (response.StatusCode != HttpStatusCode.Created) {
                throw new Exception(response.ReasonPhrase);
            }
            var responseBody = response.Content.ReadAsStringAsync()
                    .GetAwaiter().GetResult();
            return JObject.Parse(responseBody);
        }

        public JObject CreateTimelineEntry(
                long timelineID,
                string mimeType,
                byte[] content,
                Dictionary<string, string> metadata) {
            var url = String.Format(
                    "{0}timeline/{1}/entry", baseUrl, timelineID);
            var method = "post";
            var query = new Dictionary<string, string>();

            var bodyObj = new JObject();
            bodyObj["mimeType"] = mimeType;
            bodyObj["content"] = Convert.ToBase64String(content);
            var metadataArr = new JArray();
            foreach (var e in metadata) {
                var metadataObj = new JObject();
                metadataObj["key"] = e.Key;
                metadataObj["value"] = e.Value;
                metadataArr.Add(metadataObj);
            }
            bodyObj["metadata"] = metadataArr;
            var body = JsonConvert.SerializeObject(bodyObj);
            var nonce = Auth.Utils.generateNonce();
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var signature = Auth.RsaSigner.sign(
                    key,
                    method,
                    url,
                    query,
                    username,
                    body,
                    nonce,
                    timestamp);

            var authHeader = CreateAuthHeader(nonce, signature, timestamp);

            var bodyContent = new StringContent(
                    body, System.Text.Encoding.UTF8, "application/json");
            var req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Headers.Add("Authorization", authHeader);
            req.Content = bodyContent;
            var response = client.SendAsync(req).GetAwaiter().GetResult();
            if (response.StatusCode != HttpStatusCode.Created) {
                throw new Exception(response.ReasonPhrase);
            }
            var responseBody = response.Content.ReadAsStringAsync()
                    .GetAwaiter().GetResult();
            return JObject.Parse(responseBody);
        }

        public JArray ReadTimelineEntries(long timelineID) {

            var url = String.Format(
                    "{0}timeline/{1}/entry/", baseUrl, timelineID);
            var method = "get";
            var query = new Dictionary<string, string>();
            var body = "";
            var nonce = Auth.Utils.generateNonce();
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var signature = Auth.RsaSigner.sign(
                    key,
                    method,
                    url,
                    query,
                    username,
                    body,
                    nonce,
                    timestamp);

            var authHeader = CreateAuthHeader(nonce, signature, timestamp);

            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add("Authorization", authHeader);
            var response = client.SendAsync(req).GetAwaiter().GetResult();
            if (response.StatusCode != HttpStatusCode.OK) {
                throw new Exception(response.ReasonPhrase);
            }
            var responseBody = response.Content.ReadAsStringAsync()
                    .GetAwaiter().GetResult();
            return JArray.Parse(responseBody);
        }

        public JObject ReadTimelineEntry(long timelineID, long entryID) {

            var url = String.Format(
                    "{0}timeline/{1}/entry/{2}/", baseUrl, timelineID, entryID);
            var method = "get";
            var query = new Dictionary<string, string>();
            var body = "";
            var nonce = Auth.Utils.generateNonce();
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var signature = Auth.RsaSigner.sign(
                    key,
                    method,
                    url,
                    query,
                    username,
                    body,
                    nonce,
                    timestamp);

            var authHeader = CreateAuthHeader(nonce, signature, timestamp);

            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add("Authorization", authHeader);
            var response = client.SendAsync(req).GetAwaiter().GetResult();
            if (response.StatusCode != HttpStatusCode.OK) {
                throw new Exception(response.ReasonPhrase);
            }
            var responseBody = response.Content.ReadAsStringAsync()
                    .GetAwaiter().GetResult();
            return JObject.Parse(responseBody);
        }

        public byte[] ReadTimelineEntryContent(long timelineID, long entryID) {

            var url = String.Format(
                    "{0}timeline/{1}/entry/{2}/content/",
                    baseUrl,
                    timelineID,
                    entryID);
            var method = "get";
            var query = new Dictionary<string, string>();
            var body = "";
            var nonce = Auth.Utils.generateNonce();
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var signature = Auth.RsaSigner.sign(
                    key,
                    method,
                    url,
                    query,
                    username,
                    body,
                    nonce,
                    timestamp);

            var authHeader = CreateAuthHeader(nonce, signature, timestamp);

            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add("Authorization", authHeader);
            var response = client.SendAsync(req).GetAwaiter().GetResult();
            if (response.StatusCode != HttpStatusCode.OK) {
                throw new Exception(response.ReasonPhrase);
            }
            return response.Content.ReadAsByteArrayAsync()
                    .GetAwaiter().GetResult();
        }

        private string CreateAuthHeader(
                string nonce, string signature, long timestamp) {
            return String.Format("chronicle-user-hmac "
                    + "auth_nonce=\"{0}\", "
                    + "auth_signature=\"{1}\", "
                    + "auth_signature_method=\"RSA-SHA512\", "
                    + "auth_timestamp=\"{2}\", "
                    + "auth_username=\"{3}\", "
                    + "auth_version=\"1.0\"",
                    nonce, signature, timestamp, username);
        }

    }

}
