﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Crypto = Org.BouncyCastle.Crypto;

namespace Chronicle.Api.V1.Auth {

    public abstract class Utils {

        public static string generateNonce() {
            var r = new Random((int)DateTimeOffset.UtcNow.ToUnixTimeSeconds());
            var bytes = new byte[32];
            r.NextBytes(bytes);
            return Convert.ToBase64String(bytes);
        }

        internal static string quote(string s) {
            return Uri.EscapeDataString(s).Replace("%2F", "/");
        }

        internal static string buildString(
                string method,
                string url,
                Dictionary<string, string> query,
                string body,
                string nonce,
                long timestamp,
                string username = null,
                string token_id = null) {
            var buffer = new StringBuilder();
            buffer.Append(method.ToUpper())
                    .Append("&")
                    .Append(quote(url))
                    .Append("&");

            var iquery = new Dictionary<string, string>(query);
            iquery.Add("auth_signature_method", "RSA-SHA512");
            iquery.Add("auth_timestamp", String.Format("{0}", timestamp));
            if (!String.IsNullOrEmpty(username)) {
                iquery.Add("auth_username", username);
            } else {
                iquery.Add("auth_token_id", token_id);
            }
            iquery.Add("auth_version", "1.0");
            iquery.Add("auth_nonce", nonce);
            var e = iquery.ToDictionary(p => quote(p.Key), p => quote(p.Value))
                    .OrderBy(p => p.Value)
                    .OrderBy(p => p.Key);

            var queryBuffer = new StringBuilder();
            var first = e.First();
            queryBuffer.Append(first.Key);
            queryBuffer.Append("=");
            queryBuffer.Append(first.Value);
            foreach (var p in e.Skip(1)) {
                queryBuffer.Append("&");
                queryBuffer.Append(p.Key);
                queryBuffer.Append("=");
                queryBuffer.Append(p.Value);
            }
            buffer.Append(quote(queryBuffer.ToString()));
            buffer.Append("&");
            buffer.Append(quote(body));

            return quote(buffer.ToString());
        }

    }

    public class RsaSigner {

        public static string sign(
                Crypto.Parameters.RsaPrivateCrtKeyParameters key,
                string method,
                string url,
                Dictionary<string, string> query,
                string username,
                string body,
                string nonce,
                long timestamp) {
            var b = Utils.buildString(
                    method,
                    url,
                    query,
                    body,
                    nonce,
                    timestamp,
                    username: username
            );
            var bytes = Encoding.UTF8.GetBytes(b);
            var emLen = (int) Math.Ceiling((key.Modulus.BitLength - 1) / 8.0);
            var saltLen = emLen - 64 - 2;
            var signer = new Crypto.Signers.PssSigner(
                    new Crypto.Engines.RsaEngine(),
                    new Crypto.Digests.Sha512Digest(),
                    new Crypto.Digests.Sha512Digest(),
                    saltLen);
            signer.Init(true, key);
            signer.BlockUpdate(bytes, 0, bytes.Length);
            var signature = signer.GenerateSignature();
            return Convert.ToBase64String(signature);
        }

        public static bool verify(
                Crypto.Parameters.RsaKeyParameters key,
                string signature,
                string method,
                string url,
                Dictionary<string, string> query,
                string username,
                string body,
                string nonce,
                long timestamp) {
            var b = Utils.buildString(
                    method,
                    url,
                    query,
                    body,
                    nonce,
                    timestamp,
                    username: username
            );
            var bytes = Encoding.UTF8.GetBytes(b);
            var emLen = (int) Math.Ceiling((key.Modulus.BitLength - 1) / 8.0);
            var saltLen = emLen - 64 - 2;
            var signer = new Crypto.Signers.PssSigner(
                    new Crypto.Engines.RsaEngine(),
                    new Crypto.Digests.Sha512Digest(),
                    new Crypto.Digests.Sha512Digest(),
                    saltLen);
            signer.Init(false, key);
            signer.BlockUpdate(bytes, 0, bytes.Length);
            return signer.VerifySignature(Convert.FromBase64String(signature));
        }

    }

    public class HmacSigner {

        public static string sign(
                string key,
                string method,
                string url,
                Dictionary<string, string> query,
                string token_id,
                string body,
                string nonce,
                long timestamp) {
            var b = Utils.buildString(
                    method,
                    url,
                    query,
                    body,
                    nonce,
                    timestamp,
                    token_id: token_id);

            var hmac = new Crypto.Macs.HMac(new Crypto.Digests.Sha512Digest());
            var bytes = Encoding.UTF8.GetBytes(b);
            hmac.Init(new Crypto.Parameters.KeyParameter(
                    Encoding.UTF8.GetBytes(key)));
            hmac.BlockUpdate(bytes, 0, bytes.Length);
            byte[] result = new byte[hmac.GetMacSize()];
            hmac.DoFinal(result, 0);
            return Convert.ToBase64String(result);
        }

        public static bool verify(
                string key,
                string signature,
                string method,
                string url,
                Dictionary<string, string> query,
                string token_id,
                string body,
                string nonce,
                long timestamp) {
            var b = Utils.buildString(
                    method,
                    url,
                    query,
                    body,
                    nonce,
                    timestamp,
                    token_id: token_id);

            var hmac = new Crypto.Macs.HMac(new Crypto.Digests.Sha512Digest());
            var bytes = Encoding.UTF8.GetBytes(b);
            hmac.Init(new Crypto.Parameters.KeyParameter(
                    Encoding.UTF8.GetBytes(key)));
            hmac.BlockUpdate(bytes, 0, bytes.Length);
            byte[] result = new byte[hmac.GetMacSize()];
            hmac.DoFinal(result, 0);
            return Convert.ToBase64String(result).Equals(signature);
        }

    }

}
